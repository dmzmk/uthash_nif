-module(uthash).

-on_load(load_nif/0).
-export([new/0]).
-export([delete/2]).
-export([insert/3]).
-export([find/2]).
-export([info/1]).
-export([to_list/1]).
-export([count/1]).


load_nif() ->
    PrivDir = case code:priv_dir(?MODULE) of
        {error, _} ->
            EbinDir = filename:dirname(code:which(?MODULE)),
            AppPath = filename:dirname(EbinDir),
            filename:join(AppPath, "priv");
        Path ->
            Path
    end,
    erlang:load_nif(filename:join(PrivDir, "uthash_nif"), 0).


new() ->
    exit(nif_not_loaded).


delete(_Key, _Hash) ->
    exit(nif_not_loaded).


insert(_Key, _Value, _Hash) ->
    exit(nif_not_loaded).


find(_Key, _Hash) ->
    exit(nif_not_loaded).


info(_Hash) ->
    exit(nif_not_loaded).


to_list(_Hash) ->
    exit(nif_not_loaded).


count(_Hash) ->
    exit(nif_not_loaded).
