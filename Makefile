PROJECT = uthash_nif
PROJECT_DESCRIPTION = uthash nif
PROJECT_VERSION = 0.0.7

EUNIT_OPTS = verbose


compile: all


devel: all shell


clean::
	@rm -f c_src/env.mk

include erlang.mk
