#include "uthash.h"
#include "erl_nif.h"


#undef uthash_malloc
#undef uthash_free

#define uthash_malloc(size) enif_alloc(size)
#define uthash_free(ptr, size) enif_free(ptr)


ErlNifResourceType* RES_TYPE;
ERL_NIF_TERM atom_ok;
ERL_NIF_TERM atom_error;
ERL_NIF_TERM atom_alloc;
ERL_NIF_TERM atom_not_found;


typedef struct uthash_elem {
    char *key;
    char *value;
    size_t value_size;
    UT_hash_handle hh;
} uthash_elem;


static void destructor(ErlNifEnv* env, void *obj)
{
    uthash_elem *s, *tmp, *hash= *((void **) obj);
    HASH_ITER(hh, hash, s, tmp){
        HASH_DEL(hash, s);
        enif_free((void *)s->key);
        enif_free((void *)s->value);
        enif_free(s);
    };
}


static int load(ErlNifEnv* env, void** priv_data, ERL_NIF_TERM load_info)
{
    atom_ok = enif_make_atom(env, "ok");
    atom_error = enif_make_atom(env, "error");
    atom_alloc = enif_make_atom(env, "alloc");
    atom_not_found = enif_make_atom(env, "not_found");
    int flags = ERL_NIF_RT_CREATE;
    RES_TYPE = enif_open_resource_type(env, "uthash", "uthash", destructor, flags, NULL);
    if (RES_TYPE == NULL) return -1;
    return 0;
}


static char* get_binary_value(ErlNifEnv *env, ERL_NIF_TERM term, size_t *len)
{
    ErlNifBinary bin;
    char *result = NULL;
    enif_inspect_binary(env, term, &bin);
    result = enif_alloc(bin.size + 1);
    memcpy(result, bin.data, bin.size);
    result[bin.size] = '\0';
    if (len != NULL) *len = bin.size;
    return result;
}


static uthash_elem* get_hash_pointer(ErlNifEnv* env, const ERL_NIF_TERM term)
{
    void **resource;
    enif_get_resource(env, term, RES_TYPE, (void **)&resource);
    return (uthash_elem *)(* resource);
}

static void update_resource_pointer(ErlNifEnv *env, ERL_NIF_TERM term, uthash_elem *hash){
    void **resource;
    enif_get_resource(env, term, RES_TYPE, (void **)&resource);
    *resource = (void *)hash;
}


// exported functions


static ERL_NIF_TERM new(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    ERL_NIF_TERM ret;
    void **resource = enif_alloc_resource(RES_TYPE, sizeof(void *));
    // hash pointer
    *resource = NULL;
    ret = enif_make_resource(env, resource);
    enif_release_resource(resource);
    return enif_make_tuple2(env, atom_ok, ret);
}


static ERL_NIF_TERM delete(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    char *key;
    uthash_elem *hash, *elem;
    if((argc != 2) || !(enif_is_binary(env, argv[0])))
        return enif_make_badarg(env);
    hash = get_hash_pointer(env, argv[1]);
    key = get_binary_value(env, argv[0], NULL);
    HASH_FIND_STR(hash, (const char *)key, elem);
    if (elem) {
        HASH_DEL(hash, elem);
        enif_free((void *)elem->key);
        enif_free((void *)elem->value);
        enif_free(elem);
        update_resource_pointer(env, argv[1], hash);
    }
    enif_free(key);
    return atom_ok;
}


static ERL_NIF_TERM insert(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    char *key;
    uthash_elem *hash, *elem;
    if ((argc != 3) || !(enif_is_binary(env, argv[0]) && enif_is_binary(env, argv[1])))
        return enif_make_badarg(env);
    hash = get_hash_pointer(env, argv[2]);
    // check if key if present first
    key = get_binary_value(env, argv[0], NULL);
    HASH_FIND_STR(hash, (const char *)key, elem);
    if (elem){
        HASH_DEL(hash, elem);
        enif_free((void *)elem->key);
        enif_free((void *)elem->value);
        enif_free(elem);
    }
    elem = enif_alloc(sizeof(uthash_elem));
    elem->key = key;
    elem->value = get_binary_value(env, argv[1], &(elem->value_size));
    HASH_ADD_KEYPTR(hh, hash, elem->key, strlen(elem->key), elem);
    update_resource_pointer(env, argv[2], hash);
    return atom_ok;
}


static ERL_NIF_TERM find(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    char *key;
    uthash_elem *elem, *hash;
    ERL_NIF_TERM ret;
    ErlNifBinary bin;
    if ((argc != 2) || (!enif_is_binary(env, argv[0])))
        return enif_make_badarg(env);
    hash = get_hash_pointer(env, argv[1]);
    key = get_binary_value(env, argv[0], NULL);
    HASH_FIND_STR(hash, (const char*)key, elem);
    if (!elem) {
        ret =enif_make_tuple2(env, atom_error, atom_not_found);
    } else {
        enif_alloc_binary(elem->value_size, &bin);
        memcpy(bin.data, elem->value, elem->value_size);
        ret = enif_make_tuple2(env, atom_ok, enif_make_binary(env, &bin));
    }
    enif_free((void *)key);
    return ret;
}


static ERL_NIF_TERM info(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    uthash_elem *hash = get_hash_pointer(env, argv[0]);
    return enif_make_tuple2(env, atom_ok, enif_make_uint64(env, (unsigned long int)hash));
}


static ERL_NIF_TERM to_list(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[])
{
    uthash_elem *hash = get_hash_pointer(env, argv[0]), *elem, *tmp;
    ErlNifBinary bin;
    ERL_NIF_TERM key, value, list = enif_make_list(env, 0);
    HASH_ITER(hh, hash, elem, tmp){
        enif_alloc_binary(elem->value_size, &bin);
        memcpy(bin.data, elem->value, elem->value_size);
        value = enif_make_binary(env, &bin);
        enif_alloc_binary(strlen(elem->key), &bin);
        memcpy(bin.data, elem->key, strlen(elem->key));
        key = enif_make_binary(env, &bin);
        list = enif_make_list_cell(env, enif_make_tuple2(env, key, value), list);
    }
    return list;
}


static ERL_NIF_TERM count(ErlNifEnv *env, int agc, const ERL_NIF_TERM argv[])
{
    uthash_elem *hash = get_hash_pointer(env, argv[0]);
    return enif_make_uint(env, HASH_COUNT(hash));
}


static ErlNifFunc nif_funcs[] = {
    {"new", 0, new},
    {"delete", 2, delete},
    {"insert", 3, insert},
    {"find", 2, find},
    {"info", 1, info},
    {"count", 1, count},
    {"to_list", 1, to_list}
};


ERL_NIF_INIT(uthash, nif_funcs, load, NULL, NULL, NULL)
