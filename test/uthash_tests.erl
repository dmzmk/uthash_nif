-module(uthash_tests).

-include_lib("eunit/include/eunit.hrl").


uthash_test_() ->
    {timeout, 30, [
        {"checking data insertion (up to 50k elements, generated randomly, length up to 1024 bytes)", fun() ->
            Seq = lists:usort([
                Y || _ <- lists:seq(1, 50000),
                    Len <- [crypto:rand_uniform(1, 1025)],
                    X <- [crypto:rand_bytes(Len)],
                    Y <- [<< <<Y:8>> || <<Y:8>> <= X, Y /= 0 >>],
                    Y /= <<>>
            ]),
            {ok, H} = uthash:new(),
            lists:foreach(fun(X) -> ok = uthash:insert(X, X, H)end, Seq),
            lists:foreach(fun(X) -> ?assertEqual({ok, X}, uthash:find(X, H)) end, Seq),
            ToList = uthash:to_list(H),
            KVList = [{X, X} || X <- Seq],
            ?assertEqual(lists:sort(uthash:to_list(H)), lists:sort([{X, X} || X <- Seq]))
        end}
    ]}.
